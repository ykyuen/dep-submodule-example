package main
import (
  "database/sql"
  "fmt"
  "log"

  _ "github.com/go-goracle/goracle"
)
func main() {

  condb, errdb := sql.Open("goracle", "oracle://<db_connection>")
  if errdb != nil {
    fmt.Println(" Error open db:", errdb.Error())
  }
  var (
    sysdate string
  )
  rows, err := condb.Query(`SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS') "NOW" FROM DUAL`)
  if err != nil {
    log.Fatal(err)
  }
  for rows.Next() {
    err := rows.Scan(&sysdate)
    if err != nil {
      log.Fatal(err)
    }
    log.Println(sysdate)
  }
  defer condb.Close()
}
